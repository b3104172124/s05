/* The UserException class is a custom exception used to handle user-related errors in the application. It allows for better error handling and provides specific error messages for user-related exceptional situations. */
package com.zuitt.wdc044_s01.exceptions;

public class UserException extends Exception {
    public UserException(String message){
        super(message);
    }
}


