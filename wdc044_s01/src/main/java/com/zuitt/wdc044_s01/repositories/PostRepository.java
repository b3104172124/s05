package com.zuitt.wdc044_s01.repositories;

import com.zuitt.wdc044_s01.models.Post;
import com.zuitt.wdc044_s01.models.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
// By annotating it with @Repository, it becomes a Spring-managed repository component that handles data access operations for the "Post" entity//
public interface PostRepository extends CrudRepository <Post, Object>{
    Iterable<Post> findByUser(User user);
    // by extending CrudRepository, PostRepository has inherited its predefined methods for Creating, Retrieving, updating and deleting records
}
